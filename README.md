# Negometrix Forms
<h4 align="center">
  The final project for graduation of Telerik Academy Alpha .NET January 2020. 
 </h4> 
 <h4 align="center">
 Assignment from [Negometrix](https://www.negometrix.com/bg/).
  </h4> 
<table>
<h2 align="center">Negometrix Forms 

</h2>
</table>

<table>
<tr>
<h4 align="center">Negometrix Forms is based on <a href="https://dotnet.microsoft.com/apps/aspnet" target="_blank">ASP.NET Core MVC 3.1</a></h4>
</tr>
</table>

## Features
- [x] View all created forms inside the website
- [x] Search and answer any available form 
- [x] Send form to e-mail
- [x] Answer text and option questions, upload files to document questions and download them when you review the answers
- [x] You can edit, view answers and delete any form that you own

## Built with
- [ASP.NET Core 3.1](https://dotnet.microsoft.com/apps/aspnet)
- [Microsoft SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)
- [Microsoft Entity Framework Core 2.1.14](https://docs.microsoft.com/en-us/ef/core/what-is-new/)

## Tested with
- [MSTest](https://dotnet.microsoft.com/apps/aspnet)
- [MOQ 4.13.1](https://www.nuget.org/packages/moq/)
- [Microsoft Entity Framework Core InMemory](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore.InMemory)

## Trello
https://trello.com/b/o1phHOVC/negometrix-forms

## Authors

<table>
  <tr>
    <td align="center">
     <a href="https://gitlab.com/PeterBoj">
      <img src="https://secure.gravatar.com/avatar/deddc088125f1d4e12d1e9729f91aeba?s=800&d=identicon" width="100px;" alt="Diyan Lyubchev"/>
      <br />
      <sub><b>Petar Bojilov</b></sub>
       <br />
      <sub><b>Team Member</b></sub>
     </a>
     <br />
 </td>
     
<td align="center">
     <a href="https://gitlab.com/dmxice97">
      <img src="https://gitlab.com/uploads/-/system/user/avatar/4129164/avatar.png?" width="100px;" alt="Boycho Dzhorgov"/>
      <br />
      <sub><b>Kristiyan Topchiyski</b></sub>
       <br />
      <sub><b>Team Member</b></sub>
     </a>
     <br />
</td>
  </tr>
</table>

## Mentor

<table>
  <tr>
<td align="center">
     <a href="https://github.com/AnastasKosstow">
      <img src="https://avatars2.githubusercontent.com/u/45877133?s=400&u=0bdc30c643825cdc52efd3de4b3a2aaea47dff93&v=4" width="100px;" alt="Teodor Peev"/>
      <br />
      <sub><b>Atanas Kostov</b></sub>
     </a>
</td>
  </tr>
</table>

## Technical Trainers

<table>
  <tr>
<td align="center">
     <a href="https://gitlab.com/radkostanev">
      <img src="https://gitlab.com/uploads/-/system/user/avatar/4128703/avatar.png?" width="100px;" alt="Edward Nikolaev"/>
      <br />
      <sub><b>Radko Stanev</b></sub>
     </a>
</td>

<td align="center">
     <a href="https://gitlab.com/kstanoev">
      <img src="https://gitlab.com/uploads/-/system/user/avatar/3256776/avatar.png?" width="100px;" alt="Kiril Stanoev"/>
      <br />
      <sub><b>Kiril Stanoev</b></sub>
     </a>
</td>    
  </tr>
</table>