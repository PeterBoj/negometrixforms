﻿using NF.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NF.Service.Mappers
{
    public static class DocumentPathMapper
    {
        public static DocumentPathDTO ToDto(this DocumentPath model)
        {
            var modelDto = new DocumentPathDTO
            {
                Id = model.Id,
                DocumentQuestion=model.DocumentQuestion,
                DocumentQuestionId=model.DocumentQuestionId,
                Path=model.Path,
            };
            return modelDto;
        }

        public static ICollection<DocumentPathDTO> ToDto(this ICollection<DocumentPath> documentModel)
        {
            var documentModelDto = documentModel?.Select(s => s.ToDto()).ToList();
            return documentModelDto;
        }

        public static DocumentPath FromDto(this DocumentPathDTO documentPathDTO)
        {
            var documentModel = new DocumentPath
            {
                Id=documentPathDTO.Id,
                DocumentQuestion = documentPathDTO.DocumentQuestion,
                DocumentQuestionId = documentPathDTO.DocumentQuestionId,
                Path = documentPathDTO.Path,

            };

            return documentModel;
        }
        public static ICollection<DocumentPath> FromDto(this ICollection<DocumentPathDTO> documentPath)
        {
            var documentPathDTO = documentPath?.Select(s => s.FromDto()).ToList();
            return documentPathDTO;
        }

    }
}

