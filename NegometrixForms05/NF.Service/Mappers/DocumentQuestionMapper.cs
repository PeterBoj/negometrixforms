﻿using NF.Models.Models;
using NF.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NF.Service.Mappers
{
    public static class DocumentQuestionMapper
    {
        public static DocumentQuestionDTO ToDto(this DocumentQuestion documentQuestion)
        {
            var documentQuestionDto = new DocumentQuestionDTO
            {
                Id = documentQuestion.Id,
                Name = documentQuestion.Name,
                FileNumbers = documentQuestion.FileNumbers,
                FormId = documentQuestion.FormId,
                FileSize = documentQuestion.FileSize,
                DocumentPath = documentQuestion.DocumentPath,
                IsRequired = documentQuestion.IsRequired,
                Paths = documentQuestion.Paths?.ToDto(),


            };
            return documentQuestionDto;
        }

        public static ICollection<DocumentQuestionDTO> ToDto(this ICollection<DocumentQuestion> documentQuestion)
        {
            var documentQuestionDto = documentQuestion?.Select(s => s.ToDto()).ToList();
            return documentQuestionDto;
        }

        public static DocumentQuestion FromDto(this DocumentQuestionDTO documentQuestionDto)
        {
            var documentQuestion = new DocumentQuestion
            {
                Id = documentQuestionDto.Id,
                Name = documentQuestionDto.Name,
                FileNumbers = documentQuestionDto.FileNumbers,
                FormId = documentQuestionDto.FormId,
                FileSize = documentQuestionDto.FileSize,
                Paths = documentQuestionDto.Paths?.FromDto(),
                IsRequired=documentQuestionDto.IsRequired,

            };

            return documentQuestion;
        }
        public static ICollection<DocumentQuestion> FromDto(this ICollection<DocumentQuestionDTO> documentQuestionDto)
        {
            //var documentQuestion = documentQuestionDto?.Select(s => s.FromDto()).ToList();
            var documentQuestion = documentQuestionDto?.Select(FromDto).ToList();
            return documentQuestion;
        }
    }
}
