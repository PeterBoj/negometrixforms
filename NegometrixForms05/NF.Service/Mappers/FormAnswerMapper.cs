﻿using NF.Models.Models.FormModel.QuestionModels;
using NF.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NF.Service.Mappers
{
    public static class FormAnswerMapper
    {
        public static FormAnswerDTO ToDto(this FormAnswers questionForm)
        {
          

            var questionFormDTO = new FormAnswerDTO
            {
                Id = questionForm.Id,
                FormId = questionForm.FormId,
                AnsweredBy = questionForm.AnsweredBy,
                AnswerDate = questionForm.AnswerDate,
                IsAnonymous = questionForm.IsAnonymous,
                IsDeleted = questionForm.IsDeleted,
                DeletedOn = questionForm.DeletedOn,
                AnseredUserName = questionForm.AnseredUserName,
                FormName = questionForm.FormName,
                OptionQuestionAnswers = questionForm.OptionQuestionAnswers,
                TextQuestionAnswers = questionForm.TextQuestionAnswers,
                DocumentQuestionAnswers = questionForm.DocumentQuestionAnswers

            };
            return questionFormDTO;
        }
        public static ICollection<FormAnswerDTO> ToDto(this ICollection<FormAnswers> questionForm)
        {
            var questionFormDTO = questionForm.Select(q => q.ToDto()).ToList();
            return questionFormDTO;
        }
        public static FormAnswers FromDto(this FormAnswerDTO questionFormDTO)
        {
            try
            {
                var questionForm = new FormAnswers
                {
                    Id = questionFormDTO.Id,
                    FormId = questionFormDTO.FormId,
                    AnsweredBy = questionFormDTO.AnsweredBy,
                    AnswerDate = questionFormDTO.AnswerDate,
                    IsAnonymous = questionFormDTO.IsAnonymous,
                    IsDeleted = questionFormDTO.IsDeleted,
                    DeletedOn = questionFormDTO.DeletedOn,
                    AnseredUserName = questionFormDTO.AnseredUserName,
                    FormName = questionFormDTO.FormName,
                    OptionQuestionAnswers = questionFormDTO.OptionQuestionAnswers,
                    TextQuestionAnswers = questionFormDTO.TextQuestionAnswers,
                    DocumentQuestionAnswers = questionFormDTO.DocumentQuestionAnswers

                };
                return questionForm;
            }
            catch (System.Exception)
            {

                throw;
            }

        }
    }
}
