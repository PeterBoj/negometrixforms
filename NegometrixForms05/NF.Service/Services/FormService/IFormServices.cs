﻿using NF.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NF.Service.Services.FormService
{
    public interface IFormServices
    {
    //    Task<bool> EditFormAsync(Guid idOldForm, FormDTO newFormDTO);
        Task<FormDTO> GetAsync(Guid id);
        Task<ICollection<FormDTO>> GetQuestionFormsAsync();
        Task<ICollection<FormDTO>> GetAllCreatedByUserNameAsync(string userName);
        Task<ICollection<FormAnswerDTO>> GetAllAnsweredByUserNameAsync(string userName);
        Task<ICollection<FormAnswerDTO>> GetAllAnswersForForm(Guid formId);
        Task<ICollection<FormDTO>> GetAllByNameAsync(string name);
        Task<FormDTO> CreateAsync(FormDTO questionFormDTO);
        Task<FormDTO> UpdateAsync(FormDTO questionFormDTO);
        Task<string> DeleteAsync(Guid id);
        Task<FormAnswerDTO> CreateAnswerFormAsync(FormAnswerDTO model, Guid AnswerUserId);
        Task<FormAnswerDTO> CreateAnswerFormAsync(FormAnswerDTO model);
        Task<bool> DeleteFormAnswer(Guid asnwerId);
        FormAnswerDTO GetAnswersForForm(Guid asnwerId);

        string GetCreatorUsername(Guid creatorId);
        FormDTO GetMostRecent();
        FormDTO GetMostAnswered();

    }
}
