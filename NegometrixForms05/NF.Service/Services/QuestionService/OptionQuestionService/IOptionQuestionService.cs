﻿using NF.Service.Dtos;
using NF.Service.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Services.QuestionService.OptionQuestionService
{
    public interface IOptionQuestionService
    {
        Task<List<Guid>> GetOptionQuestionsGuidsAsync(Guid id);
        Task<OptionsQuestionDTO> CreateAsync(OptionsQuestionDTO model);
        Task<OptionsQuestionDTO> UpdateAsync(OptionsQuestionDTO model);
        Task<string> DeleteAsync(Guid id);
    }
}
