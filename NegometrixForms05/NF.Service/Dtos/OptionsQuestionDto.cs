﻿using NF.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Service.DTOs
{
    public class OptionsQuestionDTO
    {
        public Guid Id { get; set; }
        public Guid FormId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; } 
        public bool IsAnswered { get; set; } 
        public bool IsRequired { get; set; } 
        public ICollection<OptionModelDTO> OptionModels { get; set; } 
        public bool HasMultipleAnswers { get; set; } 
    }
}
