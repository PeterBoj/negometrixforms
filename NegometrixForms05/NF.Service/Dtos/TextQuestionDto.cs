﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NF.Service.DTOs
{
    public class TextQuestionDTO
    {
        public Guid Id { get; set; }
        public Guid FormId { get; set; }

        [Required]
        public string Name { get; set; }
        public string Text { get; set; }
        public bool IsLongAnswer { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public bool IsAnswered { get; set; } = false;
        public bool IsRequired { get; set; } = false;


    }
}
