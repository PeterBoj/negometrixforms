﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.DTOs;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.TextQuestionService;
using System;
using System.Threading.Tasks;

namespace NF.Service.Tests.TextQuestionServiceTest
{
    [TestClass]
    public class CreateTextQuestion_Should
    {
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqTextQuestionDTO = new Mock<TextQuestionDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new TextQuestionService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(moqTextQuestionDTO.Object));
            }
        }
        [TestMethod]
        public async Task Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            Guid testGuid = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B029D");

            var textQuestion = new Mock<TextQuestionDTO>();

            textQuestion.Object.Id = testGuid;
            textQuestion.Object.Name = "Test";

            var toDtoStyle = textQuestion.Object;

            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new TextQuestionService(assertContext, mockDateTime.Object);
                var resultOQDTO = await sut.CreateAsync(textQuestion.Object);

                Assert.AreEqual(resultOQDTO.Id, textQuestion.Object.Id);
                Assert.AreEqual(resultOQDTO.Name, textQuestion.Object.Name);
            }
        }
    }
}
