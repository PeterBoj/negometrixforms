﻿using Microsoft.EntityFrameworkCore;
using NF.Data;
using System;

namespace NF.Service.Tests
{
    public static class Utils
    {
        public static DbContextOptions<NFContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<NFContext>()
                .UseInMemoryDatabase(databaseName).Options;

        }
        public static DateTime GetDateTime()
        {
            return DateTime.UtcNow;
        }
    }
}
