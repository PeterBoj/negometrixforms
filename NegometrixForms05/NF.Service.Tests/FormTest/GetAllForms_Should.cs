﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Models.Models;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.FormService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Service.Tests.FormTest
{
    [TestClass]
    public class GetAllForms_Should
    {
        [TestMethod]
        public async Task Get_Question_Forms()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Get_Question_Forms));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqFormDTO = new Mock<FormDTO>();
            moqFormDTO.Object.Id = Guid.NewGuid();
            moqFormDTO.Object.Description = "Description";
            moqFormDTO.Object.Name = "Name";

            var moqFormDTO2 = new Mock<FormDTO>();
            moqFormDTO.Object.Id = Guid.NewGuid();

            var moqFormDTO3 = new Mock<FormDTO>();
            moqFormDTO.Object.Id = Guid.NewGuid();

            var form = moqFormDTO.Object.FromDto();
            var form2 = moqFormDTO2.Object.FromDto();
            var form3 = moqFormDTO3.Object.FromDto();
            var listForms = new List<Form>();
            listForms.Add(form);
            listForms.Add(form2);
            listForms.Add(form3);
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);
                //add to mock database
                await assertContext.Forms.AddAsync(form);
                await assertContext.Forms.AddAsync(form2);
                await assertContext.Forms.AddAsync(form3);
                await assertContext.SaveChangesAsync();

                var resultList = await sut.GetQuestionFormsAsync();
                for (int i = 0; i < listForms.Count; i++)
                {
                    Assert.AreEqual(resultList.ToList()[i].Id, listForms[i].Id);
                }
            }
        }
    }
}
