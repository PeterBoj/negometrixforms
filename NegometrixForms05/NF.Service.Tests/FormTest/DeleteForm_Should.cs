﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.FormService;
using System;
using System.Threading.Tasks;

namespace NF.Service.Tests.FormTest
{
    [TestClass]
    public class DeleteForm_Should
    {
        [TestMethod]
        public async Task Delete_Text_Question()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Delete_Text_Question));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqFormDTO = new Mock<FormDTO>();
            moqFormDTO.Object.Id = Guid.NewGuid();
            moqFormDTO.Object.Name = "Name";

            var form = moqFormDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);
                //add to mock database
                await assertContext.Forms.AddAsync(form);
                await assertContext.SaveChangesAsync();

                var result = await sut.DeleteAsync(moqFormDTO.Object.Id);

                Assert.AreEqual(result, form.Name);
            }
        }
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqFormDTO = new Mock<FormDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqFormDTO.Object.Id));
            }
        }
        [TestMethod]
        public async Task Throw_When_This_Question_IsNotIn_Database()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_This_Question_IsNotIn_Database));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqFormDTO = new Mock<FormDTO>();
            moqFormDTO.Object.Id = Guid.NewGuid();

            var textQuestion = moqFormDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqFormDTO.Object.Id));

            }
        }
    }
}
