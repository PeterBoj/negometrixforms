﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Services.QuestionService.OptionQuestionService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Tests.OptionQuestionModelServiceTest
{
    [TestClass]
    public class CreateOptionModelService_Should
    {
       
        [TestMethod]
        public async Task Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            Guid testGuid = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B029D");

            var optionModelForQuestion = new Mock<OptionModelDTO>();

            optionModelForQuestion.Object.Id = testGuid;
            optionModelForQuestion.Object.Text = "Text";

            var toDtoStyle = optionModelForQuestion.Object;

            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);
                var resultOQDTO = await sut.CreateAsync(optionModelForQuestion.Object);

                Assert.AreEqual(resultOQDTO, true);
            }
        }
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var optionModelForQuestion = new Mock<OptionModelDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(optionModelForQuestion.Object));
            }
        }
    }
}
