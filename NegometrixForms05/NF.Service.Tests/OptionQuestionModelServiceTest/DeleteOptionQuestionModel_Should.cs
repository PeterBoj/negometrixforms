﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.OptionQuestionService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Tests.OptionQuestionModelServiceTest
{
    [TestClass]
    public class DeleteOptionQuestionModel_Should
    {
        [TestMethod]
        public async Task Delete_Text_Question()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Delete_Text_Question));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqOptionModelQuestionDTO = new Mock<OptionModelDTO>();
            moqOptionModelQuestionDTO.Object.Id = Guid.NewGuid();
            moqOptionModelQuestionDTO.Object.IsMarked = true;
            moqOptionModelQuestionDTO.Object.Text = "Text";

            var optioModelQuestion = moqOptionModelQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);
                //add to mock database
                await assertContext.Options.AddAsync(optioModelQuestion);
                await assertContext.SaveChangesAsync();

                var result = await sut.DeleteAsync(moqOptionModelQuestionDTO.Object.Id);

                Assert.AreEqual(result, optioModelQuestion.Text);
            }
        }
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqOptionModelQuestionDTO = new Mock<OptionModelDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqOptionModelQuestionDTO.Object.Id));
            }
        }
        [TestMethod]
        public async Task Throw_When_This_Question_IsNotIn_Database()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_This_Question_IsNotIn_Database));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqOptionModelQuestionDTO = new Mock<OptionModelDTO>();
            moqOptionModelQuestionDTO.Object.Id = Guid.NewGuid();

            var textQuestion = moqOptionModelQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqOptionModelQuestionDTO.Object.Id));

            }
        }
    }
}
