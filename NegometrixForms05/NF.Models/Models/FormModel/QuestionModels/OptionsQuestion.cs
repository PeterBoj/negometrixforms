﻿using NF.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NF.Models.Models
{

    public class OptionsQuestion : IEntityQuestions
    {
        public ICollection<OptionModel> OptionModels { get; set; }
        public bool HasMultipleAnswers { get; set; } = false;

        //EntityQuestions
        [Key]
        public Guid Id { get; set; }
        public Form Form { get; set; }
        public Guid FormId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; } = false;
        public bool IsAnswered { get; set; } = false;
        public bool IsRequired { get; set; } = false;
        //DateTime

        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }

    }
}
