﻿using NF.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NF.Models.Models
{
    public class TextQuestion : IEntityQuestions
    {
        //EntityQuestions
        [Key]

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public bool IsLongAnswer { get; set; } = false;
        public Form Form { get; set; }
        public Guid FormId { get; set; }
        public bool IsDeleted { get; set; } = false;
        public bool IsAnswered { get; set; } = false;
        public bool IsRequired { get; set; } = false;

        //DateTime

        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
