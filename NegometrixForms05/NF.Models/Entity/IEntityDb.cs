﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Models.Entity
{
    public interface IEntityDb
    {
        public DateTime ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Description { get; set; }
        public Guid CreatorId { get; set; }
    }
}
