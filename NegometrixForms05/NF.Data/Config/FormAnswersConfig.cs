﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NF.Models.Models;
using NF.Models.Models.FormModel.QuestionModels;

namespace NF.Data.Config
{
    class FormAnswersConfig : IEntityTypeConfiguration<FormAnswers>
    {
        public void Configure(EntityTypeBuilder<FormAnswers> builder)
        {
            builder.HasKey(d => d.Id);
            builder.Property(d => d.FormId);
            builder.Property(d => d.AnsweredBy);
            builder.Property(d => d.IsAnonymous);
            builder.Property(d => d.AnswerDate);
            builder.Property(d => d.AnseredUserName);
            builder.Property(d => d.FormName);
            builder.Property(d => d.DeletedOn);
            builder.Property(d => d.IsDeleted);
            builder.Property(d => d.OptionQuestionAnswers);
            builder.Property(d => d.DocumentQuestionAnswers);
            builder.Property(q => q.TextQuestionAnswers);
        }
    }
   
}
