﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NF.Models.Models;


namespace NF.Data.Config
{
    internal class DocumentQuestionConfig : IEntityTypeConfiguration<DocumentQuestion>
    {
        public void Configure(EntityTypeBuilder<DocumentQuestion> builder)
        {
            builder.HasKey(d => d.Id);
            builder.HasMany(d => d.Paths).WithOne(p => p.DocumentQuestion).HasForeignKey(p => p.DocumentQuestionId);
            builder.HasOne(d => d.Form).WithMany(f => f.DocumentQuestions).HasForeignKey(x=>x.FormId);
            builder.Property(d => d.FileNumbers);
            builder.Property(d => d.FileSize).IsRequired();
            builder.Property(d => d.Name).IsRequired();
            builder.Property(d => d.IsDeleted);
            builder.Property(d => d.IsAnswered);
            builder.Property(d => d.IsRequired);
            builder.Property(q => q.CreatedOn);
            builder.Property(q => q.ModifiedOn);
        }
    }
}
