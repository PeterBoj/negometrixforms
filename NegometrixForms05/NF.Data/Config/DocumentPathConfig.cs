﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Data.Config
{
    internal class DocumentPathConfig : IEntityTypeConfiguration<DocumentPath>
    {
        public void Configure(EntityTypeBuilder<DocumentPath> builder)
        {
            builder.HasKey(d => d.Id);
            builder.HasOne(d => d.DocumentQuestion).WithMany(d => d.Paths).HasForeignKey(d => d.DocumentQuestionId);
            builder.Property(d => d.Path);
        }
    }
}
