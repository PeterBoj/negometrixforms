﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.ViewModels
{
    public class TextQuestionVM 
    {
        public Guid Id { get; set; }
        public Guid FormId { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public bool IsLongAnswer { get; set; } = false;
        public bool IsAnswered { get; set; } = false;
        public bool IsRequired { get; set; } = false;

    }
}
