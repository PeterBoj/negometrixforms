﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.ViewModels
{
    public class DocumentQuestionVM
    {
        public Guid Id { get; set; }
        public int FileNumbers { get; set; }
        public int FileSize { get; set; }
        public bool IsRequired { get; set; }
        public ICollection<DocumentPathVM> Paths { get; set; }
        public IFormFile Document { get; set; }
        public FormVM Form { get; set; }
        public Guid FormId { get; set; }
        public string Name { get; set; }
    }
}
