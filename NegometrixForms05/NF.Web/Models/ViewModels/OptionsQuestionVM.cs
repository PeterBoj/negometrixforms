﻿using NF.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.ViewModels
{
    public class OptionsQuestionVM
    {
        public Guid Id { get; set; }
        public Guid FormId { get; set; }
        public bool IsAnswered { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public ICollection<OptionModelVM> Options { get; set; }
        public bool IsRequired { get; set; }
        public bool HasMultipleAnswers { get; set; }

    }
}
