﻿using NF.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.AnswerViewModels
{
    public class FormAnswerAVM
    {
        public Guid Id { get; set; }
        public Guid FormId { get; set; }
        public Guid AnsweredBy { get; set; }
        public bool IsAnonymous { get; set; }
        public DateTime AnswerDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string AnseredUserName { get; set; }
        public string FormName { get; set; }

        //Content
        public List<OptionQuestionAVM> OptionQuestionAnswers { get; set; } = new List<OptionQuestionAVM>();
        public List<DocumentQuestionAVM> DocumentQuestionAnswers { get; set; } = new List<DocumentQuestionAVM>();
        public List<TextQuestionAVM> TextQuestionAnswers { get; set; } = new List<TextQuestionAVM>();
    }
}
