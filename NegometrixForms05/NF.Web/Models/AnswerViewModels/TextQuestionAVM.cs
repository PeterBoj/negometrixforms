﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.AnswerViewModels
{
    public class TextQuestionAVM
    {
        public bool IsAnswered { get; set; }
        public string Name { get; set; }
        public string TextAnswer { get; set; }

    }
}
