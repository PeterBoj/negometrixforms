﻿using NF.Models.Models;
using NF.Service.Dtos;
using NF.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Mappers
{
    public static class OptionModelMapper
    {
        public static OptionModelDTO ToDto(this OptionModelVM optionModel)
        {
            var optionModelDto = new OptionModelDTO
            {
                Id = optionModel.Id,
                OptionsQuestionId = optionModel.OptionsQuestionId,
                Text = optionModel.Text,
                IsMarked = optionModel.IsMarked,
            };
            return optionModelDto;
        }

        public static ICollection<OptionModelDTO> ToDto(this ICollection<OptionModelVM> optionModel)
        {
            var optionModelDto = optionModel.Select(s => s.ToDto()).ToList();
            return optionModelDto;
        }

        public static OptionModelVM FromDto(this OptionModelDTO optionmodelDTO)
        {
            var optionModel = new OptionModelVM
            {
                Id = optionmodelDTO.Id,
                OptionsQuestionId = optionmodelDTO.OptionsQuestionId,
                Text = optionmodelDTO.Text,
                IsMarked = optionmodelDTO.IsMarked,
            };

            return optionModel;
        }
        public static ICollection<OptionModelVM> FromDto(this ICollection<OptionModelDTO> optionModel)
        {
            var optionModelVM = optionModel.Select(s => s.FromDto()).ToList();
            return optionModelVM;
        }
    }
}
