﻿using NF.Service.DTOs;
using NF.Service.Mappers;
using NF.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Mappers
{
    public static class OptionsQuestionMapper
    {
        public static OptionsQuestionDTO ToDto(this OptionsQuestionVM optionsQuestionVM)
        {
            var optionsQuestionDto = new OptionsQuestionDTO
            {
                Id = optionsQuestionVM.Id,
                Name = optionsQuestionVM.Name,
                IsAnswered = optionsQuestionVM.IsAnswered,
                FormId=optionsQuestionVM.FormId,
                IsRequired=optionsQuestionVM.IsRequired,
                OptionModels = optionsQuestionVM.Options?.ToDto(),
                HasMultipleAnswers=optionsQuestionVM.HasMultipleAnswers,
            };
            return optionsQuestionDto;
        }

        public static ICollection<OptionsQuestionDTO> ToDto(this ICollection<OptionsQuestionVM> optionsQuestion)
        {
            var optionsQuestionDto = optionsQuestion.Select(s => s.ToDto()).ToList();
            return optionsQuestionDto;
        }

        public static OptionsQuestionVM FromDto(this OptionsQuestionDTO optionsQuestionDTO)
        {
            var optionsQuestion = new OptionsQuestionVM
            {
                Id = optionsQuestionDTO.Id,
                Name = optionsQuestionDTO.Name,
                IsAnswered = optionsQuestionDTO.IsAnswered,
                FormId=optionsQuestionDTO.FormId,
                IsRequired = optionsQuestionDTO.IsRequired,
                Options=optionsQuestionDTO.OptionModels?.FromDto(),
                HasMultipleAnswers=optionsQuestionDTO.HasMultipleAnswers,
            };

            return optionsQuestion;
        }
    }
}
